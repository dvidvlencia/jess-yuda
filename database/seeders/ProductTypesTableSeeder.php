<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // masks
        DB::table('product_types')->insert([
            'name' => 'Mascarilla'
        ]);
        DB::table('product_types')->insert([
            'name' => 'Gorra'
        ]);

    }
}
