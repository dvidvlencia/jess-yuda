<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 1
        ]);

        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 1
        ]);

        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 2
        ]);

        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 2
        ]);

        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 3
        ]);
        DB::table('orders')->insert([
            'folio' => '123_123',
            'is_wholesale' => true,
            'client_id' => 3
        ]);
    }
}
