<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('clients')->insert([
            'name' => 'C. David',
            'last_names' => 'Valencia Romero',
            'phone' => '123_123',
            'celphone' => '123_123',
        ]);

        DB::table('clients')->insert([
            'name' => 'La Jess :p',
            'last_names' => 'S',
            'phone' => '123_123',
            'celphone' => '123_123',
        ]);

        DB::table('clients')->insert([
            'name' => 'Otra Jess :p',
            'last_names' => 'Otros APs',
            'phone' => '123_123',
            'celphone' => '123_123',
        ]);
    }
}
