<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //print_r($request);
        $type = $request->input('type');

        if(!empty($type)){
            $products = DB::table('products')
                ->where('type_id', '=', $type)
                ->get();
            return view('product.list', ['products' => $products]);
        } else {
            return view('product.list', ['products' => Product::all()]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('product.create', []);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = new Product();

        $str = Str::random(10);
        $extension = $request->image->extension();
        $file_name = $str . "." . $extension;
        $request->image->storeAs('/public/images', $file_name);

        $product->name =  $request->input('name');
        $product->description =  $request->input('description');
        $product->type_id =  $request->input('type_id');
        $product->image =  $file_name;
        $product->save();
        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully');

        #$request->image->storeAs('images', 'filename.jpg');
        #$product->save($request->all());
        #if ($request->hasFile('image')) {
        #    //  Let's do everything here
        #    if ($request->file('image')->isValid()) {
        #        //
        #        $validated = $request->validate([
        #            'name' => 'string|max:40',
        #            'image' => 'mimes:jpeg,png|max:1014',
        #        ]);
        #        $extension = $request->image->extension();
        #        $request->image->storeAs('/public', $validated['name'].".".$extension);
        #        $url = Storage::url($validated['name'].".".$extension);
        #        $file = File::create([
        #           'name' => $validated['name'],
        #            'url' => $url,
        #        ]);
        #        Session::flash('success', "Success!");
        #        return \Redirect::back();
        #    }
        #}
        #abort(500, 'Could not upload image :(');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        return view('product.edit', ['product' => Product::find($product->id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $request->validate([
            'name' => 'required'
        ]);

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $existing_file = $product->image;
                $path_existing_file =  "/public/images/" . $existing_file;
                Storage::delete($path_existing_file);

                $str = Str::random(10);
                $extension = $request->image->extension();
                $file_name = $str . "." . $extension;
                $request->image->storeAs('/public/images', $file_name);
                
                $product->name =  $request->input('name');
                $product->description =  $request->input('description');
                $product->type_id =  $request->input('type_id');
                $product->image =  $file_name;
                $product->save();
            }
        } else {
            $product->update($request->all());
        }

        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $existing_file = $product->image;
        $path_existing_file =  "/public/images/" . $existing_file;
        
        Storage::delete($path_existing_file);
        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Se ha eliminado correctamente');
    }
}
