<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Ordenes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- INIT CONTENT --}}
                <div class="flex justify-center py-2 px-2">

                    <div class=" w-3/4 rounded px-6"> {{-- bg-gray-100 --}}
                        <div class="border-l-4 border-indigo-400 -ml-6 pl-6 flex items-center justify-between my-4">
                            {{-- <div class="font-semibold text-gray-800">
                                Ordenes
                            </div> --}}
                            <div class="text-indigo-400">See all</div>
                        </div>
                        {{-- <hr class="-mx-6" /> --}}
                      


                        <table class="table-auto">
                            <thead>
                                <tr>
                                    <th class="w-1/2 px-4 py-2">Cliente</th>
                                    <th class="w-1/4 px-4 py-2">Folio</th>
                                    <th class="w-1/4 px-4 py-2">Telefono</th>
                                    <th class="w-1/4 px-4 py-2">Tipo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td class="border px-4 py-2">{{ $order->name }}</td>
                                        <td class="border px-4 py-2">{{ $order->folio }}</td>
                                        <td class="border px-4 py-2">{{ $order->phone }}</td>
                                        <td class="border px-4 py-2">
                                            @if ($order->is_wholesale)
                                                <span
                                                    class="flex rounded-full bg-indigo-500 text-white uppercase px-2 py-1 text-xs font-bold mr-3">
                                                    {{ 'Mayoreo' }}
                                                </span>
                                            @else
                                                <span
                                                    class="flex rounded-full bg-pink-500 text-white uppercase px-2 py-1 text-xs font-bold mr-3">
                                                    {{ 'Menudeo' }}
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>

                {{-- <form id="deleteForm"
                    action="{{ route('orders.destroy', $order->id) }}" method="POST">
                    <x-jet-nav-link href="{{ route('orders.edit', $order->id) }}"
                        :active="request()->routeIs('orders')">
                        <i class="fa fa-edit fa-lg text-indigo-400 mr-4"></i>
                    </x-jet-nav-link>

                    @csrf
                    @method('DELETE')

                    <button type="submit" title="delete" style="border: none; background-color:transparent;"
                        onclick="return confirm('¿Esta seguro que desea eliminar?')">
                        <i class="text-indigo-400 fas fa-trash fa-lg text-danger"></i>

                    </button>
                </form> --}}

                {{-- END CONTENT --}}
            </div>
        </div>
    </div>

</x-app-layout>
