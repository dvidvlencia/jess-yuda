<x-landing-layout>
    <div id="page-wrapper">

        <!-- Header -->
        <section id="header">
            <div class="container">

                <!-- Logo -->
                <h1 id="logo"><a href="#">YUDA</a></h1>
                <p>Comercializadora.</p>

                <!-- Nav -->
                <nav id="nav">
                    {{-- <ul>
                        <li><a class="icon solid fa-home" href="index.html"><span>Introduction</span></a></li>
                        <li>
                            <a href="#" class="icon fa-chart-bar"><span>Dropdown</span></a>
                            <ul>
                                <li><a href="#">Lorem ipsum dolor</a></li>
                                <li><a href="#">Magna phasellus</a></li>
                                <li><a href="#">Etiam dolore nisl</a></li>
                                <li>
                                    <a href="#">Phasellus consequat</a>
                                    <ul>
                                        <li><a href="#">Magna phasellus</a></li>
                                        <li><a href="#">Etiam dolore nisl</a></li>
                                        <li><a href="#">Phasellus consequat</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Veroeros feugiat</a></li>
                            </ul>
                        </li>
                        <li><a class="icon solid fa-cog" href="left-sidebar.html"><span>Left Sidebar</span></a></li>
                        <li><a class="icon solid fa-retweet" href="right-sidebar.html"><span>Right
                                    Sidebar</span></a></li>
                        <li><a class="icon solid fa-sitemap" href="no-sidebar.html"><span>No Sidebar</span></a></li>
                    </ul> --}}
                </nav>

            </div>
        </section>

        <!-- Banner -->
        {{-- <section id="banner">
            <div class="container">
                <p>Comercial.<br />
                   e <strong>Industrial</strong>.</p>
            </div>
        </section> --}}


        <!-- Features -->
        <section id="features">
            <div class="container">
                <header>
                    <h2>Comercial e <strong>Industrial</strong>!</h2>
                </header>
                <div class="row aln-center">
                    @foreach ($products as $product)
                        <div class="col-4 col-6-medium col-12-small">

                            <!-- Feature -->
                            <section>
                                <a href="#" class="image featured">
                                    <img src="{{ URL::to('/') }}/storage/images/{{ $product->image }}" alt="" />
                                </a>
                                <header>
                                    <h3>{{ $product->name }}</h3>
                                </header>
                                <p>
                                    {{ $product->description }}
                                </p>
                            </section>

                        </div>
                    @endforeach
                    
                    <div class="col-12">
                        
                        <ul class="actions">
                            {{ $products->links('pagination::semantic-ui') }}
                            
                            {{-- <li><a href="#" class="button icon solid fa-file">Tell Me More</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <!-- Main -->
        

        <!-- Footer -->
        <section id="footer">
            <div class="container">
                <header>
                    <h2>Questions or comments? <strong>Get in touch:</strong></h2>
                </header>
                <div class="row">
                    <div class="col-6 col-12-medium">
                        <section>
                            <form method="post" action="#">
                                <div class="row gtr-50">
                                    <div class="col-6 col-12-small">
                                        <input name="name" placeholder="Name" type="text" />
                                    </div>
                                    <div class="col-6 col-12-small">
                                        <input name="email" placeholder="Email" type="text" />
                                    </div>
                                    <div class="col-12">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <a href="#" class="form-button-submit button icon solid fa-envelope">Send
                                            Message</a>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                    <div class="col-6 col-12-medium">
                        <section>
                            <p>Erat lorem ipsum veroeros consequat magna tempus lorem ipsum consequat Phaselamet
                                mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique. Curabitur
                                leo nibh, rutrum eu malesuada.</p>
                            <div class="row">
                                <div class="col-6 col-12-small">
                                    <ul class="icons">
                                        <li class="icon solid fa-home">
                                            1234 Somewhere Road<br />
                                            Nashville, TN 00000<br />
                                            USA
                                        </li>
                                        <li class="icon solid fa-phone">
                                            (000) 000-0000
                                        </li>
                                        <li class="icon solid fa-envelope">
                                            <a href="#">info@untitled.tld</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 col-12-small">
                                    <ul class="icons">
                                        <li class="icon brands fa-twitter">
                                            <a href="#">@untitled</a>
                                        </li>
                                        <li class="icon brands fa-instagram">
                                            <a href="#">instagram.com/untitled</a>
                                        </li>
                                        <li class="icon brands fa-dribbble">
                                            <a href="#">dribbble.com/untitled</a>
                                        </li>
                                        <li class="icon brands fa-facebook-f">
                                            <a href="#">facebook.com/untitled</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div id="copyright" class="container">
                <ul class="links">
                    <li>&copy; Untitled. All rights reserved.</li>
                    <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </div>
        </section>

    </div>



</x-landing-layout>
