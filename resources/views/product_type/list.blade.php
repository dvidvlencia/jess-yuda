<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tipo de productos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- INIT CONTENT --}}
                <div class="flex justify-center py-2 px-2">
                    
                    <div class="bg-gray-100 w-3/4 rounded px-6">
                        <div class="border-l-4 border-indigo-400 -ml-6 pl-6 flex items-center justify-between my-4">
                            <div class="font-semibold text-gray-800">
                                Catalogo de Tipos de productos
                            </div>
                            <x-jet-nav-link href="{{ route('product-types.create') }}"
                                :active="request()->routeIs('product-types')">
                                <i class="fa fa-plus fa-lg text-indigo-400 mr-4"></i>
                            </x-jet-nav-link>
                        </div>
                        <hr class="-mx-6" />
                        @foreach ($product_types as $product_type)
                            <div class="flex items-center justify-between my-4">
                                {{-- <div class="w-16">
                                    <img class="w-12 h-12 rounded-full" src="https://source.unsplash.com/50x50/?nature">
                                </div> --}}
                                <div class="flex-1 pl-2">
                                    <div class="text-gray-700 font-semibold">
                                        {{ $product_type->name }}
                                    </div>
                                </div>

                                
                                <div class="text-red-400">
                                    <form id="deleteForm" action="{{ route('product-types.destroy', $product_type->id) }}" method="POST">
                                        <x-jet-nav-link href="{{ route('product-types.edit', $product_type->id) }}"
                                            :active="request()->routeIs('product-types')">
                                            <i class="fa fa-edit fa-lg text-indigo-400 mr-4"></i>
                                        </x-jet-nav-link>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" title="delete"
                                            style="border: none; background-color:transparent;"
                                            onclick="return confirm('¿Esta seguro que desea eliminar?')">
                                            <i class="text-indigo-400 fas fa-trash fa-lg text-danger"></i>

                                        </button>
                                    </form>
                                  
                                </div>
                            </div>
                            <hr class="boder-b-0 my-4" />
                        @endforeach
                    </div>
                </div>
                {{-- END CONTENT --}}
            </div>
        </div>
    </div>

</x-app-layout>

