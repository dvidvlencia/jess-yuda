<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Editar') }} - {{ $product_type->name }}
        </h2>
    </x-slot>


    <div class="py-12">
        <div class="max-w-4xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- INIT CONTENT --}}
                <div class="flex justify-center py-2 px-2">

                    <div class="w-3/4 rounded px-6 mt-3">
                        {{-- FORM --}}
                        <form action="{{ route('product-types.update', $product_type->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <x-jet-label for="name" value="{{ __('Nombre') }}"
                                        class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" />
                                </div>
                                <div class="md:w-2/3">
                                    <input id="name"
                                        class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                        type="text" name="name" value="{{ $product_type->name }}" required />
                                </div>
                            </div>






                            <div class="flex items-center justify-end mt-4">

                                <x-jet-button class="ml-4 bg-indigo-500">
                                    {{ __('Guardar') }}
                                </x-jet-button>
                            </div>

                        </form>
                        {{-- END FORM --}}
                    </div>
                    {{-- END CONTENT --}}
                </div>
            </div>
        </div>

</x-app-layout>
