<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear') }}
        </h2>
    </x-slot>


    <div class="py-12">
        <div class="max-w-4xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- INIT CONTENT --}}
                <div class="flex justify-center py-2 px-2">

                    <div class="w-3/4 rounded px-6 mt-3">
                        {{-- FORM --}}
                        <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')

                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <x-jet-label for="name" value="{{ __('Nombre') }}"
                                        class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" />
                                </div>
                                <div class="md:w-2/3">
                                    <input id="name"
                                        class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                        type="text" name="name" required />
                                </div>
                            </div>


                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <x-jet-label for="desc" value="{{ __('Descripción') }}"
                                        class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" />
                                </div>
                                <div class="md:w-2/3">
                                    <textarea name="description" id="description" cols="30" rows="3" required class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500">
                                        
                                    </textarea>
                                </div>
                            </div>

                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <x-jet-label for="name" value="{{ __('Tipo') }}"
                                        class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" />
                                </div>
                                <div class="md:w-2/3">
                                    <select name="type_id" id="type_id" required class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500">
                                        <option value="1">Gorra</option>
                                        <option value="2">Mascarilla</option>
                                    </select>
                                </div>
                            </div>

                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <x-jet-label for="name" value="{{ __('Imagen') }}"
                                        class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" />
                                </div>
                                <div class="md:w-2/3">
                                    <input id="name"
                                        class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                        type="file" name="image" required />
                                </div>
                            </div>




                            <div class="flex items-center justify-end mt-4">

                                <x-jet-button class="ml-4 bg-indigo-500">
                                    {{ __('Guardar') }}
                                </x-jet-button>
                            </div>

                        </form>
                        {{-- END FORM --}}
                    </div>
                    {{-- END CONTENT --}}
                </div>
            </div>
        </div>

</x-app-layout>
