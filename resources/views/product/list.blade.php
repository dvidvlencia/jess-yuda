<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Productos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- INIT CONTENT --}}
                <div class="flex justify-center py-2 px-2">

                    <div class="bg-gray-100 w-3/4 rounded px-6">
                        <div class="border-l-4 border-indigo-400 -ml-6 pl-6 flex items-center justify-between my-4">
                            <div class="font-semibold text-gray-800">
                                Catalogo de productos
                            </div>
                            <div>
                                <form id="deleteForm" action="{{ route('products.index') }}" method="GET">
                                    {{-- @csrf --}}
                                    @method('GET')
                                    <select name="type" class="bg-gray-200 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500">
                                        <option value="1">Gorra</option>
                                        <option value="2">Mascarilla</option>
                                    </select>
                                    <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                        <i class="fa fa-search fa-lg text-indigo-400 mr-4"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="text-indigo-400">

                                <x-jet-nav-link href="{{ route('products.create') }}"
                                    :active="request()->routeIs('products')">
                                    <i class="fa fa-plus fa-lg text-indigo-400 mr-4"></i>
                                </x-jet-nav-link>
                            </div>
                        </div>
                        <hr class="-mx-6" />
                        @foreach ($products as $product)
                            <div class="flex items-center justify-between my-4">
                                <div class="w-30">
                                    <img class="w-20 h-20" src="{{ URL::to('/') }}/storage/images/{{$product->image}}">
                                </div>
                                <div class="flex-1 pl-2">
                                    <div class="text-gray-700 font-semibold">
                                        {{ $product->name }}
                                    </div>
                                    <div class="text-gray-600 font-thin py-2 px-2">
                                        {{ $product->description }}
                                    </div>
                                </div>

                                <div>

                                    <form id="deleteForm" action="{{ route('products.destroy', $product->id) }}" method="POST">
                                        <x-jet-nav-link href="{{ route('products.edit', $product->id) }}"
                                            :active="request()->routeIs('products')">
                                            <i class="fa fa-edit fa-lg text-indigo-400 mr-4"></i>
                                        </x-jet-nav-link>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" title="delete"
                                            style="border: none; background-color:transparent;"
                                            onclick="return confirm('¿Esta seguro que desea eliminar?')">
                                            <i class="text-indigo-400 fas fa-trash fa-lg text-danger"></i>

                                        </button>
                                    </form>
                                </div>

                                <div class="text-indigo-400">

                                </div>
                            </div>
                            <hr class="boder-b-0 my-4" />
                        @endforeach
                    </div>
                </div>
                {{-- END CONTENT --}}
            </div>
        </div>
    </div>
</x-app-layout>
php artisan db:seed --class=ProductsTableSeeder